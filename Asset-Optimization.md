Before we push assets, we optimize them using a set of tools provided in this repository.

# Images

Image files in the repository are optimized using either
[gifsicle](https://www.npmjs.com/package/gifsicle),
[pngquant](https://www.npmjs.com/package/pngquant) or
[svgo](https://www.npmjs.com/package/svgo).

## NPM Commands

- `npm run $ optimize.gif <file path>`: Optimize given GIF file.
- `npm run $ optimize.png <file path>`: Optimize given PNG file.
- `npm run $ optimize.svg <file path>`: Optimize given SVG file.

# Movies

Movies are provided in the mp4 container format and are optimized using [ffmpeg](https://ffmpeg.org/). This is not installed via `npm install`, but has to be installed separately.

## NPM Commands

For the npm command to run successfully, the `ffmpeg`  binary has to be available in the `PATH`.

```bash
npm run $ optimize.mp4 <file path>
```
