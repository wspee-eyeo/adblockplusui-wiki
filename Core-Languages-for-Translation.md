# Core Languages

Taking into consideration the data gathered in the last year from different teams, the languages agreed on to translate our products into, consistently and by means of a translation agency, are the following (15 in total):

**Agreed on already for different projects since more than a year ago:**

* *German*
* *French*
* *Spanish*
* *Russian*
* *Chinese Simplified*
* *Portuguese Brazil*

**Due to filter lists download data as well as visits to the ABP.org website:**

* *Italian*
* *Dutch*
* *Turkish*
* *Polish*

**Part of the top 15 as per the final result from all the gathered data:**

* *Hungarian*
* *Greek*

**Other reasons:**

* *Japanese*
* *Korean*
* *Arabic*


These may vary depending on the project, the client or any other reason.

For Websites, for example, the initial languages to focus on could be the first 6. Once possible and if also more than a specific amount of the website is already translated, the next languages on this list could be those to start translating our website into.

***This list is not set in stone and could change over time depending on changes in the market, our userbase numbers, etc.***