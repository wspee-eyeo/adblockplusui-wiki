# UI Translation Workflow

- [How to use GitLab CI](#how-to-use-gitlab-ci)
- Workflow
  1. When strings are ready:
      1. [Upload strings to XTM.](#upload-strings-to-xtm)
      2. Request and wait for agency translations.
  2. When XTM translations are ready:
      1. [Download translations from Crowdin.](#download-translations-from-crowdin)
      2. [Download translations from XTM.](#download-translations-from-xtm)
      3. [Upload strings and translations to Crowdin.](#upload-strings-and-translations-to-crowdin)  
          _**Warning:** Make sure translations for previous release have
          already been downloaded before uploading new strings._
      4. Request and wait for community translations.
  3. When Crowdin translations are ready:
      1. [Download translations from Crowdin.](#download-translations-from-crowdin)

## How to use GitLab CI

### Navigate to pipeline

1. Go to https://gitlab.com/adblockinc/ext/adblockplus/adblockplusui.
2. Select corresponding Feature branch from dropdown.
3. Click on green icon next to the latest change.

Alternatively, you can directly search for the corresponding pipeline, by
replacing `FEATURE` with the name of the Feature branch:
`https://gitlab.com/adblockinc/ext/adblockplus/adblockplusui/-/pipelines?page=1&scope=branches&ref=FEATURE`

For example, for feature-redesign, you can go to
https://gitlab.com/adblockinc/ext/adblockplus/adblockplusui/-/pipelines?page=1&scope=branches&ref=feature-redesign
and find the pipeline for the latest change there.

### Interacting with a job

After starting a job, you can:
- Look through its logs which contain useful information about errors that may
  have caused the job to fail.
- Cancel it, if the job was started by accident.
- Retry it, if a problem that caused the job to fail has been fixed.

## Workflow

### Upload strings to XTM

1. [Navigate to pipeline.](#navigate-to-pipeline)
2. Upload strings:
    - If no corresponding XTM project exists: Click on "Play" button for
      "Uploadxtm" job.
    - Otherwise: Click on "Play" button for "Updatextm" job.
3. Wait until the job has finished.
    - If the job has failed, resolve the error and click "Retry" button.

<details>
  <summary>Manual steps</summary>

  1. Checkout feature branch.
  2. Upload strings:
      - If no corresponding XTM project exists: Run
        [`xtm.create`](Utilities#xtm) command.
      - Otherwise: Run [`xtm.update`](Utilities#xtm) command.

  **Note:** TYPO changes in the original string, sometimes might need to be
  skipped. One way to do that currently is by making a new reverting commit for
  those changes before running `xtm.create` or `xtm.update` locally.
</details>

### Download translations from XTM

1. Generate translation file in XTM.
2. [Navigate to pipeline.](#navigate-to-pipeline)
3. Click on "Play" button for "Downloadxtm" job.
4. Wait until the job has finished.
    - If the job has failed, resolve the error on XTM and click "Retry" button.
5. Review newly created merge request and wait for approvals.
6. Merge changes.

<details>
  <summary>Manual steps</summary>

  1. Finish XTM workflow.
  2. Checkout feature branch.
  3. Run [`xtm.download`](Utilities#xtm) command.
  4. Create merge request as `Noissue - Downloaded translations from XTM`.
      - Add translation managers as approvers.
  5. Merge changes.
</details>

### Upload strings and translations to Crowdin

1. [Navigate to pipeline.](#navigate-to-pipeline)
2. Click on "Play" button for "Uploadcrowdin" job.
3. Wait until the job has finished.
    - If the job has failed, resolve the error and click "Retry" button.

<details>
  <summary>Manual steps</summary>

  1. Run [`crowdin.upload-strings`](Utilities#crowdin) command.
  2. Run [`crowdin.upload-translations`](Utilities#crowdin) command.
</details>

### Download translations from Crowdin

1. [Navigate to pipeline.](#navigate-to-pipeline)
2. Click on "Play" button for "Downloadcrowdin" job.
3. Wait until the job has finished.
    - If the job has failed, resolve the error on Crowdin and click "Retry"
      button.
4. Review newly created merge request and wait for approvals.
5. Merge changes.

<details>
  <summary>Manual steps</summary>

  1. Run [`crowdin.download-translations`](Utilities#crowdin) command.
  2. Create merge request as `Noissue - Downloaded translations from Crowdin`.
      - Add translation managers as approvers.
  3. Merge changes.
</details>
