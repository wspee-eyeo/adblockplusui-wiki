# Extension release workflow

1. [Create new release](#create-new-release)
2. [Work on release issues](#work-on-release-issues)
3. Only for major releases: [Work on updates page issue](#work-on-updates-page-issue)
4. Only for major releases: [Follow translation workflow][wiki-translation-workflow]
5. Feature freeze
    1. [Prepare feature freeze](#prepare-feature-freeze)
    2. **Feature freeze**  
    _Only for major releases: At least 7 days before release date_  
    _No more changes are allowed to land in release branch (excl. regression fixes or emergency fixes)._  
    _No other releases can go into feature freeze until this release has been published._
    3. [Announce feature freeze](#announce-feature-freeze)
6. [Test release](#test-release)
7. Code freeze
    1. [Prepare code freeze](#prepare-code-freeze)
    2. **Code freeze**  
    _Latest on Friday before release date_  
    _No more changes are allowed to land in release branch._
    3. [Announce code freeze](#announce-code-freeze)
8. Release
    1. [Create builds](#create-builds)
    2. [Make release](#make-release)
9. Store reviews
10. [Clean up](#clean-up)

## Create new release

**When (for major releases):** After entering code freeze of previous major release.

**When (for minor releases):** After determining that a minor release is necessary.

- Create milestone:
  - Title: Release name (e.g. `3.12`)
  - Assign milestone to all issues with ~"State::Waiting to be released" label.
- Only for major releases: Create [updates page issue][ui-template-updates]:
  - Title: `Updates page for ` + release name (e.g. `Updates page for Release 2020-1`)
  - Description: Use `update-page` template
  - Assign to milestone.
  - Add milestone description:  
    ```
    Updates: <link to updates page issue (only for major releases)>
    ```
- Update [release information][releases] accordingly.
  - Run [script][releases]: `npm run $ create.release <version>`
  - Update "milestones.created", "scope" and "version" properties in manifest.json.
  - Only for major releases: Update "announcements.updates" property in manifest.json.
- Update [release history][wiki-history] accordingly.

## Work on release issues

- [Follow development workflow][wiki-development-workflow] until ~"State::Waiting to be released" label is set.

## Work on updates page issue

**When:** At least one week before requesting final translations.

- Collect feedback about which issues should make it into updates page.
- Finalize which issues should make it into updates page.
- Create [spec][spec] MR for updates page issue for describing changes to the [update-specific parts](#update-specific-changes) of the updates page.
- Wait until ~"State::Waiting to be released" label is set.

### Update-specific changes

The updates page should list mainly changes that are visible to users or fixes for problems that have a significant impact on users. Each major update should include the following changes:

Changes to the updates notification:
- Title
- Message (incl. link for opening the updates page)

Changes to the updates page content:
- Hero image
- Title
- Subtitle
- Improvements (optional)
  - Title
  - Description
  - Documentation link (optional)
  - Image or muted video (optional)
  - Image/video description (only if image or video exists)
- Fixes (optional)
  - Title
  - Description
  - Documentation link (optional)
  - Image or muted video (optional)
  - Image/video description (only if image or video exists)

## Prepare feature freeze

**When:** After determining that the completed changes make up a good release candidate (e.g. based on amount, value).

- Check that there are issues for all agreed on changes.
- Assign milestone to all issues with ~"State::Waiting to be released" label.
- Verify all issues are ready (see also [definition of done][wiki-definition-done]):
  - Ignore unmerged issues with ~"NotReleaseBlocking" label.
  - Has ~"State::Waiting to be released"?
  - References all relevant merge requests under "Related merge requests" or issue which fixes the problem?
  - Has "Hints for testers" section, if necessary?
  - Has "Hints for translators" section, if there were changes to translation files?
- Update repository:
  - Pull master branch.
  - Pull release branch and rebase it onto master branch.
  - Pull next branch and rebase it onto release branch.
  - Merge next branch into release branch.
- Close GitLab issues and remove ~"State::Waiting to be released" label.
- Update [release information][releases] accordingly.
  - Update "approvals.scope" and "milestones.featurefreeze" properties in manifest.json.

## Announce feature freeze

- Create and publish internal announcement in [intranet blog][blog-internal]:
  - Update [release information][releases] accordingly.
    - Populate notes.json.  
      _**Important:** Any confidential information should not be pushed before the release has been made public._
    - Update "announcements.internal" property in manifest.json.
  - Run [script][releases]: `npm run $ create.announcement <VERSION> xhtml internal`
  - Open generated announcement file and copy its content into a new blog post.
- Notify stakeholders in respective channels ([eyeo][channel-eyeo], [other][channel-other]).
  - Broadcast message: `we have entered feature freeze for Adblock Plus {version}: {link}`
  - Link for eyeo stakeholders: release page
  - Link for other stakeholders: internal announcement
- Request review for release notes.
  - Run [script][releases]: `npm run $ create.announcement <VERSION> xhtml external`
  - Open generated announcement file and copy its content into a new [Drive document][notes-draft].
  - Request review from relevant stakeholders by adding a comment to the document.
  - Update [release information][releases] accordingly.
    - Update "announcements.notes-draft" property in manifest.json.

## Test release

- If it hasn't yet, run "webext" CI pipeline job on latest release branch commit.
- Request starting to test release branch.
- Sync up with QA once per week.
- Wait for QA LGTM.

## Prepare code freeze

- Check back with QA to get final QA LGTM.
- Verify all release-related issues are ready (see [Prepare feature freeze](#prepare-feature-freeze)).
- Close GitLab issues and remove ~"State::Waiting to be released" label.
- Wait for LGTMs for release notes.
- Update [release information][releases] accordingly.
  - Update "approvals.notes", "approvals.qa", "milestones.codefreeze" and "version" properties in manifest.json.
- Move release notes to [external blog][blog-external].
  - Run [script][releases]: `npm run $ create.announcement <VERSION> txp external`
  - Open generated announcement file and copy its content into a new blog post with the following meta data:
    - Excerpt: `<txp:body/>`
    - Status: Draft
    - Category 1: adblock plus
    - Category 2: adblock plus chrome
    - Section: releases
- Check whether any third-party issues are confidential and ask to make them public.
- Check whether any issues are confidential and verify that they can be made public at the time of the release.
- Update repository:
  - Pull release branch.
  - Pull master branch.
  - Fast-forward merge master branch onto release branch.
  - Update version string:
    - Modify "version" in adblockpluschrome/build/config/base.mjs file.
    - Commit with message `Noissue - Releasing Adblock Plus <VERSION>`.
    - Add tag to commit using `git tag -a -m '' <VERSION>`.
    - Push new commit to master branch.
    - Push tag.
  - Fast-forward merge release branch onto master branch and push it.
  - Rebase next branch:
    - Notify team of upcoming rebase.
    - Set "Allowed to merge" and "Allowed to push" GitLab settings to "No one".
    - Pull next branch and rebase it onto release branch.
    - Set "Allowed to push" GitLab setting to "Maintainers".
    - Enable "Allowed to force-push" GitLab setting.
    - Force-push next branch.
    - Disable "Allowed to force-push" GitLab setting.
    - Set "Allowed to merge" and "Allowed to push" GitLab settings to "Developers + Maintainers".
    - Notify team of successful rebase and request updating of open merge requests.

## Announce code freeze

- Only for major releases: Update [release information][releases] accordingly.
  - Update "milestones.release" in manifest.json with preliminary [release date](#determine-release-date).
- Only for major releases: Add release date to internal announcement.
- Only for major releases: Notify stakeholders in respective channels ([eyeo][channel-eyeo], [other][channel-other]).
  - Broadcast message: `we have set the release date for Adblock Plus {version} to {date}: {link}`
  - Link for eyeo stakeholders: release page
  - Link for other stakeholders: internal announcement
- Check whether release notes need to be updated and, if necessary, ask for changes to be reviewed.

### Determine release date

For major releases: Immediately after QA LGTM but only on Tuesdays or Wednesdays.

For minor releases: Immediately after QA LGTM but only on Tuesdays, Wednesdays or Thursdays.

## Create builds

- Verify latest CI pipeline for master branch succeeded.
- Create build files:  
  ```sh
  # Create fresh clone
  git clone https://gitlab.com/adblockinc/ext/adblockplus/adblockplusui.git
  cd adblockplusui
  git checkout master
  # Make sure dependencies are up-to-date
  npm run submodules:update-with-snippets
  npm install
  cd adblockpluschrome
  # Create Chromium ZIP file
  npx gulp build -t chrome -c release
  # Create Firefox XPI file
  npx gulp build -t firefox -c release
  # Create source code TAR.GZ file
  npx gulp source
  ```
- Verify that automatically generated JavaScript bundles in builds are smaller than 4MB.
- Verify that builds generated from source code file have the same file size as those generated from repository:  
  ```sh
  npm install
  cd adblockpluschrome
  npx gulp build -t chrome -c release
  npx gulp build -t firefox -c release
  ```
- Manually test Chrome and Firefox extension.
  - Test update scenario.
    - Unpack previous release build from [downloads repository][downloads].
    - Install it as unpacked extension.
    - Change settings.
      - Disable Acceptable Ads.
      - Add second blocking list.
      - Add custom filter.
    - Unpack current release build into the same directory.
    - Reload extension.
    - Check the following:
      - Any expected update behavior is executed (e.g. updates notification is shown).
      - No unexpected update behavior is executed (e.g. first-run page shouldn't be shown).
      - Acceptable Ads should be disabled.
      - Second blocking list should be enabled.
      - Custom filter should be enabled.
  - Verify that sitekey filters work.
    - Go to http://zins.de/.
    - Check that ads are not shown.
    - Enable Acceptable Ads.
    - Reload page.
    - Check that ads are shown.
  - Uninstall extension.

## Make release

**When:** [Release date](#determine-release-date)

- Publish to downloads.adblockplus.org via [downloads repository][downloads].
  - Create merge request for adding **XPI file** and **ZIP file** with the message `Noissue - Releasing Adblock Plus <VERSION> for Chrome, Firefox`.
  - Merge commit after receiving all required approvals.
- Publish to [CWS][store-cws].
  - If necessary, update meta data before publishing.
  - Check missing information.
  - Upload build file and publish (see [details](#upload-cws)).
  - Verify that review has started.
- Publish to [AMO][store-amo].
  - Upload build and source code file and publish (see [details](#upload-amo)).
  - Check validation warnings/errors.
  - Verify that review has started.
- Publish to [Microsoft store][store-ms].
  - Upload build file and publish (see [details](#upload-microsoft-store)).
  - Verify that review has started.
- Publish to [Opera store][store-opera].
  - Upload build file and publish (see [details](#upload-opera-store)).
  - Verify that review has started.
- Make confidential issues public, if there are any.
- Close milestone.
- Publish release notes:
  - Status: Live
- Create [GitLab release][gitlab-releases].
  - Run [script][releases]: `npm run $ create.announcement <VERSION> md external`
  - Open generated announcement file and copy its content into a new GitLab release for the respective tag.
  - Assign the respective milestone, tag and title to the GitLab release.
- Update [release information][releases] accordingly.
  - Update "announcements.notes" and "milestones.release" properties in manifest.json.
  - Only for major releases: Update "announcements.public" property in manifest.json.
- Update [release history][wiki-history].
- Notify stakeholders in respective channels ([eyeo][channel-eyeo], [other][channel-other]).
  - Broadcast message: `Adblock Plus {version} has been published: {link to public release notes}`

### Upload: AMO

1. Click "Upload new version" button.
2. Upload **XPI file**.
3. Click "Continue" button.
4. Select "Yes" to indicate that source code needs to be submitted.
5. Upload **TAR.GZ file**.
6. Click "Continue" button.
7. Ignore input fields.
8. Click "Submit version" button.

### Upload: CWS

1. In "Package" tab:
    1. Upload **ZIP file**.
    2. Verify that there have been no changes to permissions compared to prior version.
2. In "Store listing" tab: Click "Submit for review" button.

### Upload: Microsoft store

1. Click on "Update" button.
2. Click on "Replace" and upload **ZIP file**.
3. Verify that only the following permissions are listed:  
  `tabs, <all_urls>, contextMenus, webRequest, webRequestBlocking, webNavigation, storage, unlimitedStorage, notifications`
4. Click on "Continue" button.
5. Click on "Publish" button.
6. Ignore input fields and click on "Publish" button.

### Upload: Opera store

1. In "Versions" tab: Upload **ZIP file**.
2. Verify that only the following permissions are listed:
    - Access to all hosts
    - Add extra functionality to DevTools
    - contentSettings API
    - contextMenus API
    - management API
    - notifications API
    - storage API
    - tabs API
    - unlimitedStorage API
    - webNavigation API
    - webRequest API
    - webRequestBlocking API
3. Upload **TAR.GZ file** to Google Drive and change its sharing setting to "Anyone with the link".
4. Copy URL into "Extension source code URL (required only for Opera moderators)" field.
5. Confirm updated URL by clicking on green checkmark button next to it.
6. Click on "Submit changes".

## Clean up

**When:** After release is live on all stores.

- Remove note about reviews from release notes.
- Reply to any open release notes comments.
- Update [release information][releases] accordingly.
  - Update "approvals.stores" and "milestones.closed" properties in manifest.json.
- Notify stakeholders in respective channels ([eyeo][channel-eyeo], [other][channel-other]).
  - Broadcast message: `Adblock Plus {version} is now live on all stores`



[blog-external]: http://adblockplus.org/textpattern
[blog-internal]: https://www.notion.so/adblock/Release-notes-internal-8e286c1ded5643399bda79b7bcff9027
[channel-eyeo]: https://mattermost.eyeo.com/eyeo/channels/adblock-extension-releases
[channel-other]: https://app.slack.com/client/T01R10CDEJY/C01TMC4PM0E
[downloads]: https://gitlab.com/eyeo/adblockplus/downloads
[gitlab-releases]: https://gitlab.com/adblockinc/ext/adblockplus/adblockplusui/-/releases
[notes-draft]: https://drive.google.com/drive/folders/1upVVa6RV9oSsdcoPFVmgOvNg3AbybjCz?usp=sharing
[releases]: https://gitlab.com/adblockinc/ext/adblockplus/releases
[spec]: https://gitlab.com/adblockinc/ext/adblockplus/spec
[store-amo]: https://addons.mozilla.org/en-US/developers/addons
[store-cws]: https://chrome.google.com/webstore/developer/dashboard
[store-ms]: https://partner.microsoft.com/en-us/dashboard/microsoftedge/overview
[store-opera]: https://addons.opera.com/developer/
[ui-template-updates]: https://gitlab.com/adblockinc/ext/adblockplus/adblockplusui/-/issues/new?issuable_template=update-page
[wiki-definition-done]: https://gitlab.com/adblockinc/ext/adblockplus/adblockplusui/-/wikis/definition-done
[wiki-development-workflow]: https://gitlab.com/adblockinc/ext/adblockplus/adblockplusui/-/wikis/development-workflow
[wiki-history]: https://gitlab.com/adblockinc/ext/adblockplus/adblockplusui/-/wikis/releases
[wiki-translation-workflow]: https://gitlab.com/adblockinc/ext/adblockplus/adblockplusui/-/wikis/translation-workflow
