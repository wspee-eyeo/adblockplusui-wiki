# Issue lifecycle

- [Templates](#templates)
- [Stages](#stages)
- [Labels](#labels)
- [Other meta data](#other-meta-data)

## Templates

Issues can be created by anyone via [this page](https://gitlab.com/adblockinc/ext/adblockplus/adblockplusui/-/issues/new), preferrably by selecting one of the provided issue templates.
| Template | Purpose |
|----------|---------|
| bug | Report a deviation between observed and the expected behavior. |
| change | Suggest a change to the expected behavior, such as new features or changes to existing features. |
| dependency | Suggest a change to one of the repository's dependencies. |
| update-page | Inform users about new, noteworthy updates in a major release. |

## Stages

 1. [Backlog](#backlog)
 2. [Accepted](#accepted)
 3. [Draft](#draft)
 6. [QA LGTM](#qa-lgtm)
 7. [Implementation](#implementation)
 8. [Implementation Review](#implementation-review)
 9. [Waiting for 3rd Party](#waiting-for-3rd-party)
10. [Feature QA](#feature-qa)
11. [Waiting to be Translated](#waiting-to-be-translated)
12. [Translation](#translation)
13. [Waiting to be released](#waiting-to-be-released)
14. [Closed](#closed)

### Backlog

Team: Assign issue to milestone. Decide the priority of the issue and whether it can be moved to ~"State::Accepted"

Anyone: Investigate issue and collect relevant data.

### Accepted

~"State::Accepted"

Anyone: Investigate issue and collect relevant data.

### Draft

~"State::Define solution"

Anyone: [Create spec merge request](https://gitlab.com/adblockinc/ext/adblockplus/spec/-/issues/new) to describe the proposed change.

Team: 

- Review or creates copy.
- Review or proposes design changes.
- Get necessary input (e.g. legal, ops, affected dependencies).

_Can be skipped if no changes to the spec are necessary or there's no need for copy, design or looping in dependencies of interested parties (e.g. legal, ops, etc.)_

### QA LGTM

~"State::QA LGTM"

Developer or tester: Approve functionality described in spec merge request from a technical perspective.

### Implementation

~"State::Implementation"

Developer:

- Assign to issue.
- Implement changes.
- Create code merge request.

### Implementation Review

~"State::Implementation review"

Reviewer:

- Assign to issue.
- Review code merge request.

Developer

- Apply reviewer's feedback.
- Add hints for testers.
- Add hints for translators.
- Merge code.
- Upload translation file changes to XTM (see [translation workflow](translation-workflow#upload-strings-to-xtm)).

### Waiting for 3rd Party

~"State::Waiting for 3rd party"

Team: Monitor progress.

_Can be skipped if the change does not depend on changes made by or feedback coming from third parties._

### Feature QA

~"State::Feature QA"

Tester:

- Assign to issue.
- Unassign developer and reviewer from issue.
- Compare implementation in Feature branch against spec.

### Waiting to be Translated

~"State::Waiting to be translated"

Translation manager:

- Unassign tester from issue.
- Wait for sufficient amount of strings to translate.

_Can be skipped if no strings changes are necessary._

### Translation

~"State::Translation"

Translation manager:

- Assign to issue.
- Follow [translation workflow](translation-workflow).

_Can be skipped if no strings changes are necessary._

### Waiting to be released

~"State::Waiting to be released"

Team:

- Wait until we have a release candidate.

Release manager:

- Verify issue is complete.
- Close issue.
- Unassign stage label.
- Assign to new milestone.

## Labels
| Label | Meaning |
|-------|---------|
| ~"Blocked" | Issue cannot be worked on until a given condition is met. |
| ~"🐛 bug" | Issue describes a deviation between observed and the expected behavior. |
| ~"goodfirstbug" | Issue does not require special knowledge about codebase and can be picked up by anyone. |
| ~"Meta topics" | Issue represents a change that spans multiple issues. |
| ~"NotReleaseBlocking" | Issue does not require changes to the extension code, such as changes to documentation, other repositories, etc. |
| ~"core change" | Issue is for or related to a core dependency update |
| ~"snippets change" | Issue is for or related to a snippets update |

## Other meta data

- Effort
  - High
  - Medium
  - Normal
  - Low
- Priority
  - High
  - Medium
  - Low

Priority is assigned to an issue based on effort vs. [impact](#weights) so that the team is mindful of what should we work on next.

## Weights

We use weights to describe the impact an issue has, either for our users or for our team.

- `1`: issues that have low or modest impact
- `4`: issues that have medium impact
- `10`: issues that have the highest impact
