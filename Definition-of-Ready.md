_**[- - STATE: Draft-  -]**_

We want to use a Definition of Ready to determine whether a Product Backlog Item is ready to be pulled into an iteration.

We'd like to follow the ideas behind the [INVEST](https://en.wikipedia.org/wiki/INVEST_(mnemonic)) acronym, but we realize that it's not always possible or meaningful.

We do not want to enforce any of the guidelines in this document because [we value individuals and interactions over processes and tools](http://agilemanifesto.org/).

# Guidelines

Following are the guidelines for the PBIs to be considered ready. Technically, we don't differentiate between PBI types in the GitLab issue tracker, but we agree that there are different types of items, and we use labels to distinguish them if possible.

## Bug Tickets

* The ticket has the "bug" label.
* The ticket contains details on how the bug happened, or steps to reproduce the bug.
* The ticket contains details on where the bug happened.

## Stories ("Change")

* There is a user story or a some background information that describes what the value of the story is.
* If UI Changes are involved, there is a link to a figma design document that has sufficiently evolved.
* The effort has been estimated.

## Tasks

* There is some background on what the value of the task is.

## All items

* Any known dependencies the item might have are noted.
