This document gives an introduction to local development.

# Prerequisites

This project is written in JavaScript. To develop locally, you will need `node` and `npm`. For details, please refer to the [project README](https://gitlab.com/adblockinc/ext/adblockplus/adblockplusui/-/blob/master/README.md).

# Linting and Formatting

We use ESLint to do linting and formatting of JavaScript files.

# Dependencies

We use `npm` as package manager. To define our dependencies, we follow the [usual conventions](https://docs.npmjs.com/cli/v8/configuring-npm/package-json#dependencies), with one exception: We treat dependencies that are not shipped with the built extension, but still have an influence on the build output (e.g. `browserify`) as `dependencies` and not as `devDependencies`.

